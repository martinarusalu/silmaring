package ee.arusalu.silmaring;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Artikkel klass
 * <p>
 * Selle klassi objektides talletatakse MediaWiki API-st saadud andmeid. Vaata
 * lisaks ka {@link Wiki} klassi, kust artikli andmed tulevad, ja
 * {@link MainActivity} klassi, kus Artikkel objekti kasutatakse.
 * <p>
 * 
 * @author Martin Arusalu
 * @see Artikkel
 * @see MainActivity
 */
public class Artikkel {

	/**
	 * Artikli String tüüpi pealkiri.
	 */
	private String pealkiri;

	/**
	 * Artikli String tüüpi sisu.
	 */
	private String sisu;

	/**
	 * Artikli String tüüpi link.
	 */
	private String link;

	/**
	 * Artikli int tüüpi ID.
	 */
	private int ID;

	/**
	 * Artikli konstruktor.
	 * 
	 * @author Martin Arusalu
	 */
	public Artikkel() {
		this.ID = Wiki.getJuhuslikId();
		try {
			JSONObject andmed = Wiki.getById(this.ID);
			this.pealkiri = andmed.getString("title");
			this.sisu = andmed.getString("extract").replace("\n", "\n\n");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		this.link = Wiki.getLinkById(this.ID);
	}

	/**
	 * Artikli ID getter.
	 * 
	 * @author Martin Arusalu
	 * @return ID int, mis on artikli ID-ks.
	 */
	public int getID() {
		return this.ID;
	}

	/**
	 * Artikli Pealkirja getter.
	 * 
	 * @author Martin Arusalu
	 * @return pealkiri String, mis on artikli pealkirjaks.
	 */
	public String getPealkiri() {
		return this.pealkiri;
	}

	/**
	 * Artikli Sisu getter.
	 * 
	 * @author Martin Arusalu
	 * @return sisu String, mis on artikli sisuks.
	 */
	public String getSisu() {
		return this.sisu;
	}

	/**
	 * Artikli lingi getter.
	 * 
	 * @author Martin Arusalu
	 * @return link String, mis on artikli lingiks.
	 */
	public String getLink() {
		return this.link;
	}

	/**
	 * Meetod, mis tagastab kuvataval kujul artikli sisu ja lingi.
	 * 
	 * @author Martin Arusalu
	 * @return sisuJaLink String, mis sisaldab artikli sisu ja kahe reavahega eraldatud artikli linki.
	 */
	public String getSisuJaLink() {
		String sisuJaLink = this.sisu + "\n\n" + this.link;
		return sisuJaLink;
	}
	
	/**
	 * Meetod, mis otsib artikli ID järgi veebist artikli ning tagastab selle Artikkel objektina.
	 * 
	 * @author Martin Arusalu
	 * @param id int, mis on otsitava artikli ID.
	 * @return artikkel Artikkel tüüpi objekt, mis saadi antud ID järgi.
	 */
	public static Artikkel getById (int id) {
		Artikkel artikkel = new Artikkel();
		
		artikkel.ID = id;
		try {
			JSONObject andmed = Wiki.getById(artikkel.ID);
			artikkel.pealkiri = andmed.getString("title");
			artikkel.sisu = andmed.getString("extract").replace("\n", "\n\n");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		artikkel.link = Wiki.getLinkById(artikkel.ID);
		
		return artikkel;
	}

}