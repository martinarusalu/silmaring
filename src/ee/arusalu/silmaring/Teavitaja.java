package ee.arusalu.silmaring;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

/**
 * Teavitaja klass
 * <p>
 * Klass, mille käivitab alarm ning mis loob uue Artikli objekti ning saadab
 * selle andmed teavitusena kasutajale.
 * </p>
 * 
 * @author Martin Arusalu
 * @see Alarm
 */
public class Teavitaja extends IntentService {
	/**
	 * Teavitaja teenuse klassi konstruktor.
	 * 
	 * @author Martin Arusalu
	 * @see Teavitaja
	 */
	public Teavitaja() {
		super("SchedulingService");
	}

	/**
	 * Meetod, mis käivitub Teavitaja teenuse käivitamisel.
	 * 
	 * @author Martin Arusalu
	 * @param tegevus Intent, ehk tegevus, mis käivitatakse
	 * @see Teavitaja
	 */
	@Override
	protected void onHandleIntent(Intent tegevus) {
		Artikkel artikkel = new Artikkel();
		SharedPreferences seadistused = getSharedPreferences("ee.arusalu.silmaring", MODE_PRIVATE);
		
		Ajalugu.lisaAjalukku(seadistused, artikkel);
		
		saadaTeavitus(artikkel);
		Alarm.completeWakefulIntent(tegevus);
	}

	/**
	 * Meetod, mis saadab kasutajale teavituse uue artikli andmetega.
	 * 
	 * @author Martin Arusalu
	 * @param artikkel Artikkel, mille andmed saadetakse teavitusena kasutajale.
	 * @see Teavitaja
	 */
	private void saadaTeavitus(Artikkel artikkel) {
		NotificationManager teavituseHaldur = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		Intent pealeheActivity = new Intent(this, MainActivity.class);
		
		pealeheActivity.putExtra("pealkiri", artikkel.getPealkiri());
		pealeheActivity.putExtra("sisutekst", artikkel.getSisuJaLink());

		PendingIntent tegevus = PendingIntent.getActivity(this, 0, pealeheActivity, PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationCompat.Builder teavituseKonstruktor = new NotificationCompat.Builder(this)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(artikkel.getPealkiri())
				.setVibrate(new long[] { 0, 150 })
				.setSmallIcon(R.drawable.logosmall)
				.setAutoCancel(true)
				.setContentText("Klõpsa, et selle kohta rohkem teada saada!");

		teavituseKonstruktor.setContentIntent(tegevus);
		teavituseHaldur.notify(007, teavituseKonstruktor.build());
	}
}
