package ee.arusalu.silmaring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * MainActivity klass
 * <p>
 * Selle klassi eesmärgiks on Androidi applikatsiooni juhtimine. Vaata lisaks ka
 * {@link Artikkel} klassi, kust andmeid küsitakse ning {@link VeebiParing}
 * klassi, kus luuakse eraldi lõim veebipäringu teostamiseks
 * <p>
 * 
 * @author Martin Arusalu
 * @see Artikkel
 */
public class MainActivity extends Activity {

	/**
	 * MainActivity, et saaks viidata klasside seest (vaata muudaAjalugu meetodit).
	 */
	private MainActivity viide;

	/**
	 * Rakenduse vaate konteiner.
	 */
	private SwipeRefreshLayout konteiner;

	/**
	 * Rakenduse teksti konteiner.
	 */
	private TextView sisu;

	/**
	 * Rakenduse seadistused.
	 */
	private SharedPreferences seadistused = null;

	/**
	 * Menüü konteiner.
	 */
	private DrawerLayout mDrawerLayout;

	/**
	 * Menüü elemendid
	 */
	private ListView mDrawerList;

	/**
	 * Menüü elementide adapter.
	 */
	private ArrayAdapter<String> adapter;

	/**
	 * Ajaloo pealkirjad.
	 */
	private List<String> pealkirjadeList;

	/**
	 * Rakenduse käivitamisel teostatav meetod.
	 * 
	 * @author Martin Arusalu
	 * @param savedInstanceState Bundle, milles on kõikide Activity'te jagatud andmed.
	 * @see #otsiArtikkel()
	 * @see MainActivity
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		viide = this;

		setContentView(R.layout.activity_main);
		seadistused = getSharedPreferences("ee.arusalu.silmaring", MODE_PRIVATE);

		sisu = (TextView) findViewById(R.id.sisu);
		konteiner = (SwipeRefreshLayout) findViewById(R.id.konteiner);

		muudaAjalugu();

		sisu.setMovementMethod(new ScrollingMovementMethod());
		konteiner.setOnRefreshListener(new OnRefreshListener() {
			/**
			 * Rakenduse värskendamisel käivituv meetod.
			 * 
			 * @author Martin Arusalu
			 */
			@Override
			public void onRefresh() {
				otsiArtikkel();
			}

		});

		if (seadistused.getBoolean("firstrun", true)) {
			Alarm alarm = new Alarm();
			alarm.registreeriAlarm(this);
			seadistused.edit().putBoolean("firstrun", false).commit();
		}

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			setTitle(extras.getString("pealkiri"));
			sisu.setText(extras.getString("sisutekst"));
		} else {
			otsiArtikkel();
		}
	}

	/**
	 * Meetod, mis kontrollib võrguühenduse olemasolu ning seejärel laseb
	 * {@link VeebiParing} klassil kuvada artikli sisu.
	 * 
	 * @author Martin Arusalu
	 * @see #onCreate(Bundle)
	 * @see MainActivity
	 * @see Artikkel
	 */
	private void otsiArtikkel() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			new VeebiParing(this, -1).execute();
		} else {
			setTitle("Silmaring");
			sisu.setText("Võrguühendus puudub");
		}
	}

	/**
	 * Meetod, mille kaudu saab teistest klassidest MainActivity sisuteksti
	 * muuta. Lisaks uuendab artiklite ajalugu.
	 * 
	 * @author Martin Arusalu
	 * @param artikkel Artikkel, mille andmetega vaate sisu muudetakse.
	 */
	public void muudaSisu(Artikkel artikkel) {
		Ajalugu.lisaAjalukku(seadistused, artikkel);

		muudaAjalugu();
		
		setTitle(artikkel.getPealkiri());
		sisu.setText(artikkel.getSisuJaLink());
		konteiner.setRefreshing(false);
	}

	/**
	 * Meetod, mis loob vasakpoolsesse menüüsse ajaloo listi Kui see on juba
	 * olemas, siis värskendab seda uute andmetega.
	 * Suur osa meetodi koodist pärineb siit:
	 * http://developer.android.com/training/implementing-navigation/nav-drawer.html
	 */
	private void muudaAjalugu() {

		String[] pealkirjad = Ajalugu.getPealkirjad(seadistused);
		pealkirjadeList = new ArrayList<String>(Arrays.asList(pealkirjad));
		Collections.reverse(pealkirjadeList);

		if (adapter == null) {
			mDrawerList = (ListView) findViewById(R.id.left_drawer);
			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			adapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, pealkirjadeList);
			mDrawerList.setAdapter(adapter);

			mDrawerList.setOnItemClickListener(new OnItemClickListener() {
				@SuppressWarnings("rawtypes")
				@Override
				public void onItemClick(AdapterView parent, View view, int position, long id) {
					int artikliId = Ajalugu.getIdByPealkiri(seadistused, pealkirjadeList.get(position));
					new VeebiParing(viide, artikliId).execute();
					konteiner.setRefreshing(true);
					mDrawerList.setItemChecked(position, false);
					mDrawerLayout.closeDrawer(mDrawerList);
				}
			});

		} else {
			adapter.clear();
			adapter.addAll(pealkirjadeList);
			adapter.notifyDataSetChanged();
		}
	}

}
