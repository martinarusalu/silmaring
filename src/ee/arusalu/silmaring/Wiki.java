package ee.arusalu.silmaring;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Wiki klass
 * <p>
 * Selle klassi ülesandeks on teostada päringuid MediaWiki API külge. Vaata
 * lisaks ka {@link Artikkel} klassi, kus kasutatakse Wiki klassi meetodeid
 * <p>
 * 
 * @author Martin Arusalu
 * @see Artikkel
 */
public class Wiki {

	/**
	 * <b>Otsi juhusliku artikli ID</b>
	 * 
	 * See meetod muudab juhusliku artikli päringust saadud andmed
	 * JSONObject'iks. Saadud objektist filtreeritakse välja artikli ID ning
	 * tagastatake see. Kui tekib viga, käivitatakse funktsioon uuesti.
	 * 
	 * @author Martin Arusalu
	 * @return juhuslik int, mis saadakse MediaWiki API poole pöördudes. Suvalise artikli ID.
	 * @see #getFromUrl(String)
	 */
	public static int getJuhuslikId() {
		String Url = "https://et.wikipedia.org/w/api.php?action=query&list=random&rnnamespace=0&format=json";

		try {
			JSONObject andmed = getFromUrl(Url);
			int juhuslik = andmed.getJSONObject("query").getJSONArray("random").getJSONObject(0).getInt("id");
			return juhuslik;
		} catch (JSONException x) {
			return getJuhuslikId();
		}
	}

	/**
	 * <b>Otsi artiklit ID järgi</b>
	 * 
	 * See meetod muudab artikli päringust saadud andmed JSONObject'iks. Saadud
	 * objektist filtreeritakse välja artikli andmed (pealkiri, sisu) ja
	 * tagastatakse saadud JSONObject. Kui antud tekib viga, genereeritakse uus
	 * juhuslik ID ja otsitakse selle järgi.
	 * 
	 * @author Martin Arusalu
	 * @param ID int tüüpi artikli ID.
	 * @return artikkel JSONObject, mis sisaldab artikkli sisu ja pealkirja.
	 * @see #getFromUrl(String)
	 */
	public static JSONObject getById(int ID) {
		String Url = "https://et.wikipedia.org/w/api.php?action=query&prop=extracts&exintro=&explaintext=&format=json&pageids="
				+ ID;

		try {
			JSONObject andmed = getFromUrl(Url);
			JSONObject artikkel = andmed.getJSONObject("query").getJSONObject("pages")
					.getJSONObject(Integer.toString(ID));
			return artikkel;
		} catch (Exception x) {
			return getById(getJuhuslikId());
		}
	}

	/**
	 * <b>Otsi artikli linki ID järgi</b>
	 * 
	 * See meetod muudab artikli lingi päringust saadud andmed JSONObject'iks.
	 * Saadud objektist filtreeritakse välja artikli link ja tagastatakse saadud
	 * String. Kui antud tekib viga, genereeritakse uus juhuslik ID ja otsitakse
	 * selle järgi.
	 * 
	 * @author Martin Arusalu
	 * @param ID int tüüpi artikli ID.
	 * @return link String, mis sisaldab artikli veebi URL-i.
	 * @see #getFromUrl(String)
	 */
	public static String getLinkById(int ID) {
		String Url = "https://et.wikipedia.org/w/api.php?action=query&prop=info&format=json&inprop=url&pageids=" + ID;

		try {
			JSONObject andmed = getFromUrl(Url);
			String link = andmed.getJSONObject("query").getJSONObject("pages").getJSONObject(Integer.toString(ID))
					.getString("fullurl");
			return link;
		} catch (Exception x) {
			return getLinkById(getJuhuslikId());
		}
	}

	/**
	 * <b>Veebipäring MediaWiki API külge</b>
	 * 
	 * See meetod teostab MediaWiki API külge päringu, mis tagastab JSON
	 * struktuuriga Stringi. Saadud String muudetakse JSONObject'iks ja
	 * tagastatakse see. Kui antud tekib viga, tagastatakse tühi JSONObject.
	 * 
	 * @author Martin Arusalu
	 * @param Url String tüüpi päringu URL.
	 * @return JSONObject String, mis sisaldab päringust saadud andmeid.
	 */
	private static JSONObject getFromUrl(String Url) {
		try {
			URL wikiUrl = new URL(Url);
			URLConnection link = wikiUrl.openConnection();

			BufferedReader luger = new BufferedReader(new InputStreamReader(link.getInputStream()));
			String toores = luger.readLine();
			luger.close();

			JSONObject andmed = new JSONObject(toores);

			return andmed;
		} catch (Exception x) {
			return null;
		}
	}

}
