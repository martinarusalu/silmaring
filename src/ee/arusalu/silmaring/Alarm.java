package ee.arusalu.silmaring;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import java.util.Calendar;

/**
 * Alarm klass
 * <p>
 * Klass, mis registreerib androidi süsteemi uue alarmi. Alarm käivitub igal
 * hommikul kell 5 ning teostab uue artikli otsimise ja loob teavituse. Teavitus
 * jõuab kasutajani ekraani avamisel pärast kella viite.
 * </p>
 * 
 * @author Martin Arusalu
 * @see Teavitaja
 */
public class Alarm extends WakefulBroadcastReceiver {

	/**
	 * Süsteemne teenus, mis käivitab alarmi ka siis, kui rakendus parasjagu ei
	 * tööta.
	 */
	private AlarmManager alarmiHaldur;

	/**
	 * Tegevus, mis teostatakse alarmi käivitumisel.
	 */
	private PendingIntent alarmiTegevus;

	/**
	 * Meetod, mis käivitub alarmi käivitumisel.
	 * 
	 * @author Martin Arusalu
	 * @param context Context, milles alarm käivitatakse.
	 * @param intent Intent, mis on nõutud onRecieve meetodi parameetrina.
	 * @see Alarm
	 * @see MainActivity
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		Intent tegevus = new Intent(context, Teavitaja.class);
		startWakefulService(context, tegevus);
	}

	/**
	 * Meetod, mis registreerib androidi süsteemi alarmi igaks hommikuks kell 5.
	 * 
	 * @author Martin Arusalu
	 * @param context Context, milles alarm registreeritakse.
	 * @see Alarm
	 * @see MainActivity
	 */
	public void registreeriAlarm(Context context) {
		alarmiHaldur = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, Alarm.class);
		alarmiTegevus = PendingIntent.getBroadcast(context, 0, intent, 0);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, 05);
		calendar.set(Calendar.MINUTE, 00);
		alarmiHaldur.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmiTegevus);
	}

}
