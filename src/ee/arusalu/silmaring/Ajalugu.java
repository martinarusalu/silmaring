package ee.arusalu.silmaring;

import java.util.HashSet;
import java.util.Set;
import android.content.SharedPreferences;

/**
 * Ajalugu klass
 * <p>
 * Klass, kus salvestatakse ja küsitakse ajalugu. Kirjed salvestatakse rakenduse
 * sättesse niemga "ajalugu". Ajaloo puhul objekte ei ole. Ainult static tüüpi
 * meetodid.
 * </p>
 * 
 * @author Martin Arusalu
 */
public class Ajalugu {

	/**
	 * Meetod, mis tagastab "ajalugu" sättes sisalduvad ajaloo kirjed.
	 * 
	 * @author Martin Arusalu
	 * @param s SharedPreferences sätted, kust võetakse ajaloo kirjed.
	 * @return ajalugu String kahemõõtmeline massiiv, mis sisaldab artikleid (pealkiri, id, järjekorra number).
	 */
	public static String[][] getAjalugu(SharedPreferences s) {
		Set<String> ajaluguPref = s.getStringSet("ajalugu", new HashSet<String>());
		String[][] ajalugu = new String[ajaluguPref.size()][3];

		for (String artikliAndmed : ajaluguPref) {
			String[] eraldatud = artikliAndmed.split("\\|");
			ajalugu[Integer.parseInt(eraldatud[2])][0] = eraldatud[0];
			ajalugu[Integer.parseInt(eraldatud[2])][1] = eraldatud[1];
		}

		return ajalugu;
	}

	/**
	 * Meetod, mis tagastab "ajalugu" sättes sisalduvad ajaloo pealkirjad.
	 * 
	 * @author Martin Arusalu
	 * @param s SharedPreferences sätted, kust võetakse ajaloo kirjed.
	 * @return ajalugu String massiiv, mis sisaldab artiklite pealkirju.
	 */
	public static String[] getPealkirjad(SharedPreferences s) {
		Set<String> ajaluguPref = s.getStringSet("ajalugu", new HashSet<String>());
		String[] ajalugu = new String[ajaluguPref.size()];

		for (String artikliAndmed : ajaluguPref) {
			String[] eraldatud = artikliAndmed.split("\\|");
			ajalugu[Integer.parseInt(eraldatud[2])] = eraldatud[0];
		}

		return ajalugu;
	}

	/**
	 * Meetod, salvestab loetud artikli ajalukku.
	 * 
	 * @author Martin Arusalu
	 * @param s SharedPreferences sätted, kust võetakse ajaloo kirjed.
	 * @param artikkel Artikkel, mille andmed ajalukku salvestatakse.
	 */
	public static void lisaAjalukku(SharedPreferences s, Artikkel artikkel) {
		Set<String> ajaluguPref = s.getStringSet("ajalugu", new HashSet<String>());

		String lisaString = artikkel.getPealkiri() + "|" + artikkel.getID() + "|" + ajaluguPref.size();
		ajaluguPref.add(lisaString);

		SharedPreferences.Editor e = s.edit();
		e.clear();
		e.putStringSet("ajalugu", ajaluguPref);
		e.commit();
	}

	/**
	 * Meetod, mis kustutab kõik ajaloo kirjed.
	 * 
	 * @author Martin Arusalu
	 * @param s SharedPreferences sätted, kust võetakse ajaloo kirjed.
	 */
	public static void kustutaAjalugu(SharedPreferences s) {
		SharedPreferences.Editor e = s.edit();
		e.clear();
		e.putStringSet("ajalugu", new HashSet<String>());
		e.commit();
	}

	/**
	 * Meetod, mis otsib ajaloost antud pealkirjaga kirje ning tagastab selle
	 * int tüüpi ID.
	 * 
	 * @author Martin Arusalu
	 * @param s SharedPreferences sätted, kust võetakse ajaloo kirjed.
	 * @param pealkiri String - artikli pealkiri, mille ID-d küsitakse.
	 * @return artikliId int - otsitava artikli id.
	 */
	public static int getIdByPealkiri(SharedPreferences s, String pealkiri) {
		String[][] ajalugu = Ajalugu.getAjalugu(s);
		int artikliId = 0;

		for (int i = 0; i < ajalugu.length; i++) {
			if (ajalugu[i][0].equals(pealkiri)) {
				artikliId = Integer.parseInt(ajalugu[i][1]);
				break;
			}
		}

		return artikliId;
	}

}
