package ee.arusalu.silmaring;

import android.os.AsyncTask;

/**
 * Veebiparing klass
 * <p>
 * Klass, mis teeb uue lõime ning loob selle sees {@link Artikkel} objekti
 * (teostab veebipäringu). Seejärel kuvab loodud objekti andmed seadme
 * ekraanile.
 * </p>
 * 
 * @author Martin Arusalu
 * @see #otsiArtikkel()
 * @see MainActivity
 * @see Artikkel
 */
public class VeebiParing extends AsyncTask<String, Void, Artikkel> {

	/**
	 * Rakenduse pealeht, kuhu lähevad veebipäringust tulnud andmed
	 */
	MainActivity vaade;
	
	/**
	 * Kui ajaloost otsitakse mnda vanemat artiklit, siis on id juba olemas.
	 */
	int artikliId;

	/**
	 * Veebipäringu konstruktor
	 * 
	 * @author Martin Arusalu
	 * @param v MainActivity ehk pealeht, kuhu andmed saadetakse
	 */
	public VeebiParing(MainActivity v, int id) {
		super();
		this.vaade = v;
		this.artikliId = id;
	}

	/**
	 * Meetod, mis teostab rakenduse taustal {@link Artikkel} objekti loomise
	 * (veebipäringu). Saadud andmed saadetakse funktsiooni
	 * {@link #onPostExecute(Artikkel)}
	 * 
	 * @author Martin Arusalu
	 * @param params doInBackground meetodi nõutud parameetrid.
	 * @return artikkel {@link Artikkel} objekt
	 * @see VeebiParing
	 * @see #onPostExecute(Artikkel)
	 */
	@Override
	protected Artikkel doInBackground(String... params) {
		Artikkel artikkel;
		if (artikliId > 0) {
			artikkel = Artikkel.getById(artikliId);
		} else {
			artikkel = new Artikkel();
		}
		return artikkel;
	}

	/**
	 * Meetod, mis kuvab seadme ekraanile funktsioonist
	 * {@link #doInBackground(TextView...)} saadud andmed.
	 * 
	 * @author Martin Arusalu
	 * @param artikkel {@link Artikkel} objekt
	 * @see VeebiParing
	 * @see #doInBackground(TextView...)
	 */
	@Override
	protected void onPostExecute(Artikkel artikkel) {
		vaade.muudaSisu(artikkel);
	}

}
